<?php

namespace ApiHandler\Traits;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Trait RouteParser
 * @package ApiHandler\Traits
 */
trait RouteParser
{
    /**
     * @param ServerRequestInterface $request
     * @param string $needle
     * @return bool
     */
    private function routeContains(ServerRequestInterface $request, string $needle): bool
    {
        $path = $request->getUri()->getPath();

        $exploded = explode('/', $path);

        return in_array($needle, $exploded, true);
    }
}
