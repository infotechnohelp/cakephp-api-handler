<?php

namespace ApiHandler\Traits;

use Cake\Event\Event;
use Cake\Network\Exception\InternalErrorException;

trait ApiController
{
    /**
     * @var array
     */
    private $jsonResponse = ['data' => null, 'message' => null, 'code' => null];

    /**
     * @var int
     */
    private $statusCode = 200;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * @param      $data
     * @param null $code
     * @param null $message
     */
    protected function _setResponse($data, $code = null, $message = null)
    {
        $this->jsonResponse['data'] = $data;
        $this->jsonResponse['message'] = $message;

        if (!empty($code)) {
            $this->statusCode = $code;
        }
    }

    /**
     * @param $requestData
     * @return bool|int|null|string
     */
    private function getRequestJson($requestData)
    {
        $possiblyJson = null;
        $possiblyEmptyValue = null;

        foreach ($requestData as $key => $value) {
            $possiblyJson = $key;
            $possiblyEmptyValue = $value;
            break;
        }

        if (count($requestData) === 1 &&
            empty($possiblyEmptyValue) &&
            is_string($possiblyJson) &&
            json_decode($possiblyJson) !== null
        ) {
            return $possiblyJson;
        }

        return false;
    }

    /**
     * @return mixed
     */
    protected function _getRequestData()
    {
        $requestData = $this->getRequest()->getData();

        if ($this->getRequestJson($requestData) !== false) {
            return json_decode($this->getRequestJson($requestData), true);
        }

        return $requestData;
    }

    /**
     * @param \Cake\Event\Event $event
     *
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $action = $this->getRequest()->getParam('action');

        if (method_exists($this, $action)) {
            switch ($action) {
                case 'index':
                case 'view':
                case 'read':
                case 'get':
                    $this->getRequest()->allowMethod('get');
                    break;
                case 'create':
                case 'add':
                    $this->getRequest()->allowMethod('post');
                    $this->statusCode = 201;
                    break;
                case 'update':
                    $this->getRequest()->allowMethod(['post', 'put']);
                    break;
                case 'delete':
                    $this->getRequest()->allowMethod(['post', 'delete']);
                    break;
            }
        }

        return parent::beforeFilter($event);
    }


    /**
     * @param \Cake\Event\Event $event
     *
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if ($this->jsonResponse['data'] === null) {
            throw new InternalErrorException('Data was not set');
        }

        $this->jsonResponse['code'] = $this->statusCode;
        $this->response = $this->response->withStatus($this->statusCode);

        $this->set('jsonResponse', $this->jsonResponse);
        $this->set('_serialize', 'jsonResponse');
    }
}
