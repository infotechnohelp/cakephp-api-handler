<?php

namespace ApiHandler\Error;

use Cake\Error\ExceptionRenderer;

/**
 * Class ApiExceptionRenderer
 * @package ApiHandler\Error
 */
class ApiExceptionRenderer extends ExceptionRenderer
{
    /**
     * @return \Cake\Http\Response
     */
    public function render()
    {
        $this->controller->response = $this->controller->response->withType('json');

        $incorrectHttpResponseCodes = ['0', '23000', '42S22', '42S02' /*MYSQL*/];

        $errorCode = (!in_array((string)$this->error->getCode(), $incorrectHttpResponseCodes, true)) ?
            $this->error->getCode() :
            500;

        $this->controller->response = $this->controller->response->withStatus($errorCode);

        return $this->controller->response->withStringBody(json_encode([
            'data'    => null,
            'code'    => $errorCode,
            'message' => $this->error->getMessage(),
        ], JSON_PRETTY_PRINT));
    }
}
