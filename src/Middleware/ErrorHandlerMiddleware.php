<?php

namespace ApiHandler\Middleware;

use ApiHandler\Traits\RouteParser;
use Cake\Core\Configure;

/**
 * Class ErrorHandlerMiddleware
 * @package ApiHandler\Middleware
 */
class ErrorHandlerMiddleware extends \Cake\Error\Middleware\ErrorHandlerMiddleware
{
    use RouteParser;

    private function routeContainsApiPathPrefix($Request)
    {
        foreach (Configure::read('ApiHandler')['pathPrefixes'] as $prefix) {
            if ($this->routeContains($Request, $prefix)) {
                return true;
            }
        }

        return false;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {

        if ($this->routeContainsApiPathPrefix($request)) {
            $this->exceptionRenderer = 'ApiHandler\Error\ApiExceptionRenderer';
        }

        return parent::__invoke($request, $response, $next);
    }
}
