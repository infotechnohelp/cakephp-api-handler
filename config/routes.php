<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;

$routerCallback = function (RouteBuilder $routes) {
    $routes->prefix('api', function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    });
};

Router::scope('/', $routerCallback);

$routerCallbacks = Configure::read('routerCallbacks');

if (empty($routerCallbacks)) {
    $routerCallbacks = [];
}
Configure::write('routerCallbacks', array_merge($routerCallbacks, ['cakephp-api-handler' => $routerCallback]));