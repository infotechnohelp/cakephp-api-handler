<?php

namespace TestApp\Controller\Api;

use ApiHandler\Traits\ApiController;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Network\Exception\InternalErrorException;
use TestApp\Controller\AppController;

class MyController extends AppController
{
    use ApiController;

    public function getOne()
    {
        $this->_setResponse(1);
    }

    public function exception()
    {
        throw new InternalErrorException('Exception message');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->loadComponent('RequestHandler');
    }
}