<?php

namespace ApiHandler\Test\TestCase\Controller\Api;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Class MyControllerTest
 * @package ApiHandler\Test\TestCase\Controller\Api
 */
class MyControllerTest extends IntegrationTestCase
{
    public function testGetById()
    {
        Configure::write('ApiHandler', [
            'pathPrefixes' => ['api'],
        ]);

        $this->get('api/my/get-one');
        $this->assertResponseOk();
        $responseJson = (string)$this->_response->getBody();
        $this->assertEquals(1, json_decode($responseJson, true)['data']);
    }

    public function testException()
    {
        Configure::write('ApiHandler', [
            'pathPrefixes' => ['api'],
        ]);

        $this->get('api/my/exception');
        $this->assertResponseCode(500);
        $responseJson = (string)$this->_response->getBody();
        $this->assertEquals('Exception message', json_decode($responseJson, true)['message']);
    }
}
